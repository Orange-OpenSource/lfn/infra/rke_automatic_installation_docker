# RKE automatic installation

## Goal

This project aims to automatically install a Kubernetes pod using IDF/PDF
description files from OPNFV and few variables. At the end of the deployment, it
can trigger a deployment of ONAP via OOM method.


## Automatic deployment

Today, this code is automatically deployed on Orange POD. You can look at
`.gitlab-ci.yml` to see how it's done.

## Usage

First thing is to provide good PDF/IDF files. Current files are for example but
allow you to deploy a 1 Controller / 4 Workers POD on OpenStack.
See OPNFV PDF/IDF description file to know how to map with your needs.

You also need access to a "jumphost" that will have access to the servers (
virtual or physical). This jumphost will be use as a ssh proxy. It's actually
not needed but not really tested (without jumphost) so PR are welcome :)

### on OpenStack

in order to deploy a pod on OpenStack, you must provide your credentials (in
`group_vars/all.yaml`). The jumphost will need to be able to create project,
servers, routers with these credentials.

Then you create a (simple) inventory file where you define how to access to your
jumphost (see http://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
for more information on how to define an inventory in ansible):

```
jumphost ansible_host=YOURJUMPHOST
```

Then you can launch the different steps:

```bash
# Machine creation with right stuff and inventory creation

# the key from key_name will be created in OpenStack if needed with the value
# found in private_key_path

# rke_path is a local path on the machine (not on jumphost)

# branch is used to name the project inside OpenStack
ansible-playbook -i inventory -e branch=my_branch -e key_name=MyKey -e private_key_path=~/.ssh/id_rsa -e rke_path=. opnfv-prepare.yaml

# Retrieval of rke and configuration of it

# put http_proxy and htts_proxy if needed

# rke_path is a local path on the machine (not on jumphost)

# branch is used to name the k8s cluster.
ansible-playbook -i artifacts/inventory -e branch=my_branch -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e rke_path=. opnfv-k8s-prepare.yaml


# launch rke
cd rke
ansible-playbook -i ../artifacts/inventory -b -e docker_dns_servers_strict=no cluster.yml
```

### on Baremetal


TODO
It's roughly the same as OpenStack except you need the machines to be started.

## Work to be done

- [ ] realign with IDF/PDF new format from opnfv
- [ ] realign with baremetal deployment work from Orange
- [ ] test if direct access to the machines (no jumphost) works.

## Work that may be done

- [ ] deploy on other cloud platform than OpenStack

## Input

  - configuration files:
    - mandatory:
        - vars/pdf.yml: POD Description File
        - vars/idf.yml: POD Installer Description File
        - inventory/infra: the ansible inventory for the servers
    - optional:
        - vars/vaulted_ssh_credentials.yml: Ciphered private/public pair of key
          that allows to connect to jumphost and servers
  - Environment variables:
    - mandatory:
        - PRIVATE_TOKEN: to get the artifact
        - artifacts_src: the url to get the artifacts
        - OR artifacts_bin: b64_encoded zipped artifacts (tbd)
        - ANSIBLE_VAULT_PASSWORD: the vault password needed by ciphered ansible
          vars
    - optional:
      - ANSIBLE_VERBOSE:
          - role: verbose option for ansible
          - values: "", "-vvv"
          - default: ""
      - ENABLE_MONITORING:
          - role: add monitoring of infrastructure via prometheus or not
          - value type: boolean
          - default: False
      - WEAVE_SCOPE_VERSION
          - role: if monitoring enabled, which version of weave scope to deploy
          - value type: string
          - default: "1.9.1"
      - ENABLE_SCTP
          - role: add SCTP feature gate
          - value type: boolean
          - default: False
      - docker_version:
          - role: the docker version (in package manager format)
          - default: "5:18.09.9~3-0~debian-stretch"
      - kubernetes_release:
          - role: the helm version to deploy
          - default: v1.15.6-rancher1-2
      - rke_version:
          - role: the rke version to deploy
          - default: v1.0.0
      - helm_version:
          - role: the helm version to deploy
          - default: v2.14.3
      - kube_network_plugin:
          - role: which network plugin to use
          - default: weave
      - use_jumphost:
          - role: do we need to connect via a jumphost or not?
          - value type: boolean
          - default: "yes"
      - proxy_command:
          - role: do we need to use a proxy command to reach the jumphost or
            not?
          - value: "", "the proxy command (example: connect -S socks:1080 %h
            %p)"
          - default: ""
      - ingress_provider:
          - role: ingress provider to use. Use none to disable
          - default: nginx
      - ENABLE_ISTIO
          - role: install Istio
          - default: false
      - istio_release
          - role: Which version of Istio to Install
          - default: 1.6.3
      - istio_profile
          - role: Which profile of Istio to use for install. See
                  [Istio Documentation](https://istio.io/latest/docs/setup/additional-setup/config-profiles/).
          - default: demo
      - GANDI_API_V5_KEY
          - role: if using Gandi webhook for cert manager, mandatory API key
      - ACME_SOLVER_EMAIL
          - role: if using Cert Manager, mandatory email address.

## IDF

This roles depends heavily on configuration given from "IDF" (Installer
Description format) given.

Here's the part of an IDF with some variables set:

```yaml
os_infra:
  kubernetes:
    # If you want to create storage classes
    storage_classes:
      - name: hdd
        parameters:
          availability: nova
          type: public
        provisioner: kubernetes.io/cinder
    # If cert manager is installed, and you want to:
    # * Add a webhook (Gandi is the only one supported for now)
    # * Create Issuers in the relevant namespaces.
    # * Create Certificates in some namespaces.
    certmanager:
      webhooks:
        - name: letsencrypt-gandi
          git: https://github.com/bwolf/cert-manager-webhook-gandi.git
          namespaces:
            - istio-system
            - keycloak
          certificates:
            - name: keycloak
              namespace: keycloak
              secretName: keycloak-tls
              dnsNames:
                - "{{ keycloak_fqdn | default('keycloak.k8s3.onap.eu') }}"
    # Helm repositories to add
    helm:
      repositories:
        - name: jetstack
          url: https://charts.jetstack.io
        - name: codecentric
          url: https://codecentric.github.io/helm-charts
    # Helm charts to install
    charts:
      # NFS provisionner is a special case as it's overiffe file is configured
      # directly from some values.
      nfs-server-provisioner:
        chart: stable/nfs-server-provisioner
        namespace: nfs-server
        enabled: true
        storageClass: ssd
      # * `content` will be what's inside the override file.
      # * namespace defaults to `default` if not set.
      # * istioEnabled will set the label to have sidecar injection if True.
      #   Default is True.
      cert-manager:
        chart: jetstack/cert-manager
        namespace: cert-manager
        istioEnabled: true
        content: |
          ---
          installCRDs: true
          prometheus:
            servicemonitor:
              enabled: true

  # Do we use OpenStack features in Kubernetes?
  openstack:
    cinder: true
    lbaas: true
    octavia: true
```

## Output
  - artifacts:
    - vars/kube-config

## Monitoring

If monitoring is enabled, we first need a group `monitoring` with at least one
server. We'll install prometheus and grafana on this server and node_exporter to
all servers.
