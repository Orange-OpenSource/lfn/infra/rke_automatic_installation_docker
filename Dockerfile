FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:2.10-alpine
LABEL maintainer="David Blaisonneau <david.blaisonneau@orange.com>"

ARG VCS_REF
ARG BUILD_DATE

ENV APP /opt/rke_automatic_installation/

RUN mkdir ${APP}
COPY . $APP/

RUN apk --no-cache --update-cache add --virtual build \
        build-base \
        libffi-dev \
        python3-dev \
        openssl-dev && \
  pip install --no-cache-dir --requirement ${APP}/requirements.txt &&\
  ansible-galaxy install -r ${APP}/requirements.yml && \
  apk del build
